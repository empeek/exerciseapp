import {
    View,
    Text,
    Image,
    Navigator,
    ScrollView,
    ListView,
    TouchableHighlight,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import bindIndexToActionCreators from '../../state/question/bindIndexToActionCreators'

import {
    loadQuestions,
    setQuestion,
} from '../../state/subject/actions';

import {
    setQuestions,
    selectRadioQuestion,
    selectCheckboxQuestion
} from '../../state/question/actions';

import ShowingCurrentQuestion from './ShowingCurrentQuestion';
import NavigationBar from './NavigationBar';

import SCTypeQuestion from '../Question/SCTypeQuestion';
import MCTypeQuestion from '../Question/MCTypeQuestion';

import IconFA from 'react-native-vector-icons/FontAwesome';
import {subjectStyles} from './styles';

const subject = require('../../jsons/subject.json');

const SCDispatchProperties =
    index =>
        dispatch => bindActionCreators(
            bindIndexToActionCreators({selectRadioQuestion}, index),
            dispatch);

const MCDispatchProperties =
    index =>
        dispatch => bindActionCreators(
            bindIndexToActionCreators({selectCheckboxQuestion}, index),
            dispatch);

class Subject extends Component {
    componentDidMount() {
        this.props.loadQuestions(subject);
        this.props.setQuestions(subject.questions);
    }

    constructor() {
        super();

        this.nextQuestion = this.nextQuestion.bind(this);
        this.prevQuestion = this.prevQuestion.bind(this);
    }

    randomOrder(array) {
        let sortedArray = array.slice();

        return sortedArray.sort(function (a, b) {
            return 0.5 - Math.random()
        });
    };

    handleAnswerSelected(value, index) {
        console.log(value, index);
    }

    renderScene(route, navigator) {
        return (
            <View style={subjectStyles.content}>
                {(()=> {
                    switch (route.type) {
                        case 'single':
                            return (
                                <SCTypeQuestion
                                    choices={this.props.questions[this.props.currentQuestion].answers}
                                    title={route.question}
                                    index={this.props.currentQuestion}
                                    onSelect={this.handleAnswerSelected}
                                    {...SCDispatchProperties(this.props.currentQuestion)(this.props.dispatch)}/>
                            );
                        case 'multiple':
                            return (
                                <MCTypeQuestion
                                    choices={this.props.questions[this.props.currentQuestion].answers}
                                    title={route.question}
                                    index={this.props.currentQuestion}
                                    onSelect={this.handleAnswerSelected}
                                    {...MCDispatchProperties(this.props.currentQuestion)(this.props.dispatch)}/>
                            );
                    }
                })()}
            </View>)

    }

    nextQuestion() {
        if (this.props.currentQuestion < (this.props.numberOfQuestions - 1)) {
            this.props.setQuestion(this.props.currentQuestion + 1);
            this.refs['questionsNav'].jumpForward();
        }
    }

    prevQuestion() {
        if (this.props.currentQuestion) {
            this.props.setQuestion(this.props.currentQuestion - 1);
            this.refs['questionsNav'].jumpBack();
        }
    }

    submitAnswer() {

    }

    render() {
        return (
            <View style={subjectStyles.mainContainer}>
                {(this.props.isLoading) ?
                    <View style={subjectStyles.spinnerWrap}>
                        <ActivityIndicator size="large"/>
                    </View>
                    :
                    <View style={subjectStyles.container}>
                        <ShowingCurrentQuestion currentQuestion={this.props.currentQuestion}
                                                numberOfQuestions={this.props.numberOfQuestions}/>
                        <Navigator
                            style={subjectStyles.navWrapper}
                            ref='questionsNav'
                            configureScene={(route) => Navigator.SceneConfigs.FloatFromRight}
                            initialRoute={this.props.questions[0]}
                            initialRouteStack={this.props.questions}
                            renderScene={this.renderScene.bind(this)}/>
                        <NavigationBar prevQuestion={this.prevQuestion} nextQuestion={this.nextQuestion}
                                       currentQuestion={this.props.currentQuestion}
                                       numberOfQuestions={this.props.numberOfQuestions}/>
                    </View>
                }
            </View>
        );
    }
}

Subject.propTypes = {
    navigator: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const subject = state.get('subject');
    return {
        isLoading: subject.get('isLoading'),
        title: subject.get('title'),
        questions: state.get('questions').toJS(),
        currentQuestion: subject.get('currentQuestion'),
        numberOfQuestions: subject.get('numberOfQuestions'),
        score: subject.get('score'),
        answers: subject.get('answers'),
        completed: subject.get('completed'),
    }
}

function mapDispatchToProps(dispatch) {
    return Object.assign(
        {},
        bindActionCreators({
            loadQuestions,
            setQuestion,
            setQuestions,
        }, dispatch),
        {dispatch: dispatch})
}

export default connect(mapStateToProps, mapDispatchToProps)(Subject);
