import {
    LOAD_QUESTIONS,
    SET_CURRENT_QUESTION,
} from '../action-types';

export const loadQuestions = (subject) => ({
    type: LOAD_QUESTIONS,
    subject
});

export const setQuestion = (currentQuestion) => ({
    type: SET_CURRENT_QUESTION,
    currentQuestion
});
