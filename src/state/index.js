import chapter from './chapter/reducer';
import subject from './subject/reducer';
import home from './home/reducer';
import questions from './question/reducerCollection';

import {combineReducers} from 'redux-immutable';

const applicationReducers = {
    home,
    chapter,
    subject,
    questions,
};

function createReducer() {
    return combineReducers(applicationReducers);
}

export default createReducer;
