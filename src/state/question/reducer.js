import {
    SELECT_RADIO_QUESTION,
    SELECT_CHECKBOX_QUESTION,
} from '../action-types';
import {List, fromJS} from 'immutable';

export const INITIAL_STATE = fromJS({
    type: '',
    question: '',
    selected: -1,
    answers: []
});

const updateQuestionAnswer = (state, indexAnswer, resetSelected) =>
    state.updateIn(['answers'], items => {
        if (indexAnswer !== -1) {
            if (resetSelected) {
                return items.map((item, index) => {
                    if (indexAnswer === index) {
                        return item.set('isSelected', true)
                    } else {
                        return item.set('isSelected', false)
                    }
                })
            }
            return items.update(indexAnswer, (item) =>
                item.updateIn(['isSelected'], ic => !ic));
        }
        return items;
    });

const question = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SELECT_RADIO_QUESTION:
            return updateQuestionAnswer(state, action.indexAnswer, true);
        case SELECT_CHECKBOX_QUESTION:
            return updateQuestionAnswer(state, action.indexAnswer);
    }
};


export default question;