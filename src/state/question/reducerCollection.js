import {
    SET_QUESTIONS,
} from '../action-types';
import {List, fromJS} from 'immutable';

import question, {INITIAL_STATE as QUESTION_INITIAL_STATE} from './reducer'

const INITIAL_STATE = fromJS([
    QUESTION_INITIAL_STATE
]);

const questions = (state = INITIAL_STATE, action) => {
    if (action.type.startsWith('question/')) {
        return fromJS([
            ...state.slice(0, action.index),
            question(state.get(action.index), action),
            ...state.slice(action.index + 1)
        ])
    }
    switch (action.type) {
        case SET_QUESTIONS:
            return fromJS(action.questions.map((question) => {
                return QUESTION_INITIAL_STATE
                    .set('type', question.type)
                    .set('question', question.question)
                    .set('answers', fromJS(question.answers))
            }))
        default:
            return state
    }
};

export default questions;
