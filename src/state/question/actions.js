import {
    INCREMENT,
    DECREMENT,
    SET_QUESTIONS,
    SELECT_RADIO_QUESTION,
    SELECT_CHECKBOX_QUESTION,
} from '../action-types';

export const incrementAction = () => ({
    type: INCREMENT
});

export const decrementAction = () => ({
    type: DECREMENT
});


export const setQuestions = (questions) => ({
    type: SET_QUESTIONS,
    questions
});


export const selectRadioQuestion = (indexAnswer) => ({
    type: SELECT_RADIO_QUESTION,
    indexAnswer
});

export const selectCheckboxQuestion = (indexAnswer) => ({
    type: SELECT_CHECKBOX_QUESTION,
    indexAnswer
});
